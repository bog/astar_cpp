#ifndef GLOBAL_HHP
#define GLOBAL_HHP

/* BLOCS */
#define N_BLOC_W 50
#define N_BLOC_H 50
#define BLOC_SZ 16

/* PATH */
#define PATH_SZ (BLOC_SZ/2)
/* WINDOW */
#define WIDTH (N_BLOC_W*BLOC_SZ)
#define HEIGHT (N_BLOC_H*BLOC_SZ)
/* WEIGHT */
#define MAX_WEIGHT 1000


#endif//END GLOBAL_HHP
