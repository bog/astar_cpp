#include <Astar.hpp>
#include <memory>
#include <cassert>
#include <random>
#include <chrono>

Astar::Astar(Map &m) : m_map(m)
{
  m_rs = sf::RectangleShape(sf::Vector2f(PATH_SZ, PATH_SZ));
}

void Astar::compute(std::pair<int, int> start_pos,std::pair<int, int> end_pos)
{
  m_open.clear();
  m_close.clear();
  m_path.clear();

  std::shared_ptr<Node> start = m_map.get(start_pos.first, start_pos.second);
  std::shared_ptr<Node> current = start;
  std::shared_ptr<Node> end = m_map.get(end_pos.first, end_pos.second);

  start->weight = 0;
  m_open.push_back(current);
  
  while( true )
    {
      auto neighbors = m_map.neighbors(current->i, current->j);

      // for each neighbors
      for( std::shared_ptr<Node> &neighbor : neighbors )
	{
	  // we do not want neighbors which are
	  // in the closed list
	  auto itr_close = std::find(m_close.begin(), m_close.end(), neighbor);
	  if( itr_close != m_close.end() ){ continue; }

	  // we do not want obstacle
	  if( neighbor->type->value == MAX_WEIGHT ){ continue; }
        
	  unsigned distance_to_end = m_map.distance(neighbor->i, neighbor->j,
						    end->i, end->j);

	  unsigned distance_to_start = m_map.distance(neighbor->i, neighbor->j,
						      start->i, start->j);
							

	  auto itr_open = std::find(m_open.begin(), m_open.end(), neighbor);

	  unsigned cost = distance_to_start + distance_to_end;

	  // we need to add the neighbor in the open list
	  if( itr_open == m_open.end() )
	    {
	      neighbor->weight = cost;
	      neighbor->parent = current;
	      m_open.push_back(neighbor);
	    }
	  // the neighbor is already in the open list
	  else
	    {
	      // if we have found a better way to neighbor,
	      // we update it
	      bool updateCost = false;
	      
	      if( (*itr_open)->weight > cost )
		{
		  updateCost = true;
		}
	      
	      if( cost == (*itr_open)->weight )
		{
		  std::mt19937 rand( std::chrono::steady_clock::now().time_since_epoch().count());
		  std::uniform_int_distribution<int> uid(0, 1);
		  updateCost = static_cast<bool>( uid(rand) );
		}
	      
	      if( updateCost )
		{
		  (*itr_open)->parent = current;
		  (*itr_open)->weight = cost;
		}
	    }
	}
      
      // the current node has been explored
      m_close.push_back(current);
      
      auto itr = std::find(m_open.begin(), m_open.end(), current);
      m_open.erase( itr );

      // there is not any path from
      // start to end...
      if( m_open.empty() )
	{
	  std::cout<< "NO PATH" <<std::endl;
	  return;
	}
      
      // the new current is the min of the open list
      auto next = *(std::min_element(m_open.begin(), m_open.end(), [](std::shared_ptr<Node> const& a, std::shared_ptr<Node> const& b){
	    return a->weight < b->weight;
	  }));

      // we have found the end !
      if( next == end )
	{ 
	  // we build the path
	  std::shared_ptr<Node> c = next;
	  std::shared_ptr<Node> s = start;

	  while( c != start )
	    {
	      m_path.push_front(c);
	      c = c->parent;
	    }
	  m_path.push_front(c);

	  std::cout<< m_path.size() << " NODES PATH FOUND" <<std::endl;
	  return;
	}

      current = next;
      
    }
  
}


void Astar::display(sf::RenderWindow &window)
{
  int i =0;
  for(std::shared_ptr<Node> const& node: m_close)
    {
      int n = (i*105)/m_close.size();
      m_rs.setFillColor(sf::Color(150+n, 150+n, 0, 200));
      m_rs.setPosition(sf::Vector2f(node->j*BLOC_SZ, node->i*BLOC_SZ));
      
      float sz = PATH_SZ;
      m_rs.setSize( sf::Vector2f(sz, sz) );
      
      float offset = (BLOC_SZ-sz)/2;
      m_rs.move(sf::Vector2f(offset, offset));
      window.draw(m_rs);
      i++;
    }
    
  i=0;
  for(std::shared_ptr<Node> const& node: m_path)
    {
      m_rs.setFillColor(sf::Color::Black);
      m_rs.setPosition(sf::Vector2f(node->j*BLOC_SZ, node->i*BLOC_SZ));
      float sz = i*(3*PATH_SZ/4)/m_path.size() + PATH_SZ/4;
      m_rs.setSize( sf::Vector2f(sz, sz) );
      float offset = (BLOC_SZ-sz)/2;
      m_rs.move(sf::Vector2f(offset, offset));

      window.draw(m_rs);
      i++;
    }


}

Astar::~Astar()
{
  
}
