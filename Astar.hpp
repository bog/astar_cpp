/* Astar */
#ifndef ASTAR_HPP
#define ASTAR_HPP
#include <iostream>
#include <memory>
#include <deque>
#include <Map.hpp>

class Astar
{
public:
  Astar(Map &m);
  virtual ~Astar();

  void compute(std::pair<int, int> start_pos, std::pair<int, int> end_pos);
  void display(sf::RenderWindow &window);
protected:
  
private:
  Astar( Astar const& astar ) = delete;
  Astar& operator=( Astar const& astar ) = delete;
  
  Map &m_map;
  std::vector<std::shared_ptr<Node> > m_open;
  std::vector<std::shared_ptr<Node> > m_close;
  std::deque<std::shared_ptr<Node> > m_path;

  sf::RectangleShape m_rs;
  
  
};

#endif
