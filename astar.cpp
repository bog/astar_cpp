#include <iostream>
#include <memory>

#include <SFML/Graphics.hpp>

#include <Map.hpp>
#include <Astar.hpp>


struct Pencil
{
  sf::RenderWindow &window;
  Map &map;
  std::string type;
  std::string defaultType;

  Pencil(sf::RenderWindow &w, Map& m): window(w)
				     , map(m)
				     , type("mountain")
				     , defaultType("grass")
  {
  }

  void apply()
  {
    int i = sf::Mouse::getPosition(window).y/BLOC_SZ;
    int j = sf::Mouse::getPosition(window).x/BLOC_SZ;
    
    if( !map.exists(i, j) ){ return; }
    
    map.setType(type, i, j);
  }


  void erase()
  {
    std::string tmp = type;
    
    type = defaultType;
    apply();
    type = tmp;
  }
  
};

int main()
{
  Map map;

  sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "ASTAR");
  sf::Event event;
  
  Pencil pencil(window, map);

  Astar astar(map);

  sf::Clock computeClock;
  int computeDelay = 125;
  
  while( window.isOpen() )
    {
      while( window.pollEvent(event) )
	{
	  switch( event.type )
	    {
	    case sf::Event::Closed:
	      window.close();
	      break;
	      
	    default: break;
	    }
	}
      // KEYBINDING

      // close
      if( sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)
	  || (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) && sf::Keyboard::isKeyPressed(sf::Keyboard::C))
	  || (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) && sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
	      )
	{
	  window.close();
	}
      
      // apply
      if( sf::Mouse::isButtonPressed(sf::Mouse::Left) )
	{
	  pencil.apply();
	}

       // erase
      if( sf::Mouse::isButtonPressed(sf::Mouse::Right) )
	{
	  pencil.erase();
	}

      if( sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && computeClock.getElapsedTime().asMilliseconds() >= computeDelay )
	{
	  astar.compute(std::make_pair(0, 0), std::make_pair(N_BLOC_W/2-1, N_BLOC_H/2-1));
	  computeClock.restart();
	}

      // UPDATE 
      window.clear(sf::Color(4, 139, 154));

      // DISPLAY
      map.display(window);
      astar.display(window);
      window.display();
    }
  
  return 0;
}
