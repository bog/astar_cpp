#include <Map.hpp>
#include <cassert>
Map::Map()
{
  // TYPES
  m_types["grass"] = std::shared_ptr<NodeType>(new NodeType(sf::Color::Green, 1));
  m_types["mountain"] = std::shared_ptr<NodeType>(new NodeType(sf::Color(100, 100, 100), MAX_WEIGHT));

  // NODES
  for(int i=0; i<N_BLOC_H; i++)
    {
      for(int j=0; j<N_BLOC_W; j++)
	{
	  m_map[i][j] = std::shared_ptr<Node>(new Node(i, j));
	  m_map[i][j]->type = m_types["grass"];
	}
    }

  // GRAPHICS
  m_rs = sf::RectangleShape( sf::Vector2f(BLOC_SZ, BLOC_SZ) );
 
}

void Map::update()
{
  
}

std::shared_ptr<Node>& Map::get(int i, int j)
{
  assert(exists(i, j));

  return m_map[i][j];
}

void Map::setType(std::string type, int i,int j)
{
  assert(exists(i, j));
  
  auto itr = m_types.find(type);

  if( itr == m_types.end() )
    {
      return;
    }
  
  m_map[i][j]->type = m_types[type];
}

std::vector<std::shared_ptr<Node> > Map::neighbors(int i, int j)
{
  std::vector<std::shared_ptr<Node> > res;
  
  if( exists(i+1, j) )
    {
      res.push_back( m_map[i+1][j] );
    }
  if( exists(i-1, j) )
    {
      res.push_back( m_map[i-1][j] );
    }
  if( exists(i, j+1) )
    {
      res.push_back( m_map[i][j+1] );
    }
  if( exists(i, j-1) )
    {
      res.push_back( m_map[i][j-1] );
    }
  
  return res;
}

unsigned Map::distance(int i1, int j1, int i2, int j2)
{
  assert(exists(i1, j1));
  assert(exists(j2, j2));
  
  return abs(i1 - i2) + abs(j1 - j2);
  //return sqrt((i1-i2)*(i1-i2) + (j1-j2)*(j1-j2));
}

void Map::display(sf::RenderWindow &window)
{
    for(int i=0; i<N_BLOC_H; i++)
    {
      for(int j=0; j<N_BLOC_W; j++)
    	{
    	  std::shared_ptr<Node> node = m_map[i][j];
	  
    	  m_rs.setPosition(sf::Vector2f(j*BLOC_SZ, i*BLOC_SZ));
	  m_rs.setFillColor(node->type->color);

	  window.draw(m_rs);
    	}
    }
    
}

Map::~Map()
{
}
