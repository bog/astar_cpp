/* Map */
#ifndef MAP_HPP
#define MAP_HPP

#include <iostream>
#include <memory>
#include <unordered_map>
#include <vector>

#include <SFML/Graphics.hpp>
#include <global.hpp>

struct NodeType
{
  sf::Color color;
  unsigned value;
  
  NodeType(sf::Color c, unsigned v)
  {
    color = c;
    value = v;
  }
  
};

struct Node
{
  std::shared_ptr<NodeType> type;
  std::shared_ptr<Node> parent;
  
  int i;
  int j;
  unsigned weight;
  
  Node(int _i, int _j)
  {
    i = _i;
    j = _j;
    weight = MAX_WEIGHT;
    parent = nullptr;
    type = std::make_shared<NodeType>(sf::Color::White, MAX_WEIGHT);
  }
  
};

class Map
{
public:
  Map();
  
  void update();
  void display(sf::RenderWindow &window);

  void setType(std::string type, int i,int j);
  
  bool exists(int i, int j)
  {
    return i >= 0 && i < N_BLOC_W && j >= 0 && j < N_BLOC_H;
  }

  std::vector<std::shared_ptr<Node> > neighbors(int i, int j);
  unsigned distance(int i1, int j1, int i2, int j2);
  
  std::shared_ptr<Node>& get(int i, int j);
  virtual ~Map();
  
protected:
  
private:
  Map( Map const& map ) = delete;
  Map& operator=( Map const& map ) = delete;

  std::shared_ptr<Node> m_map[N_BLOC_H][N_BLOC_W];
  std::unordered_map<std::string, std::shared_ptr<NodeType> > m_types;
  
  sf::RectangleShape m_rs;
};

#endif
